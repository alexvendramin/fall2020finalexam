
public class Recursion {

	private static int total = 0;
	
	public static void main(String[] args) {
		
		String[] words = new String[10];
		words[0] = "afesgfd";
		words[1] = "r3wg";
		words[2] = "vyhuijok";
		words[3] = "yhuasijcn";
		words[4] = "ija29iw";
		words[5] = "feudb";
		words[6] = "24u8tiwgnjs";
		words[7] = "hwdaij";
		words[8] = "koafji";
		words[9] = "wg9eiaf";
		
		
		int n = 5;
		
		int output = 0;
		
		output = recursiveCount(words, n);
		
		System.out.println(output);
	}
	
	
	public static int recursiveCount(String[] words, int n) {
		if(n < 0) {
			return 0;
		}
		
		
		if(n % 2 == 0) {
			total++;
		}
		if(words[n].contains("q")) {
			total++;
		}
		recursiveCount(words, n-1);
		return total;
	}

}
