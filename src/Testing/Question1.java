package Testing;

public class Question1 {
	public static void main(String[] args) {
		int x = foo(5);
		System.out.println(x);
	}
	
	public static int foo(int n) {
		if(n <= 0) {
			return Math.abs(n);
		}
		
		int x = foo(n-2);
		System.out.println(x);
		int y = foo(n-1);
		System.out.println(y);
		int z = foo(n-1);
		System.out.println(z);
		return x+y+z;
	}
}
