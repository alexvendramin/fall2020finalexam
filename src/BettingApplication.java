import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import java.util.Random;


public class BettingApplication extends Application{

	private int wallet = 250;
	private static Random random = new Random();
	
	public void start(Stage stage) {
		Group root = new Group();
		Scene scene = new Scene(root, 600, 400);
		scene.setFill(Color.BLUE);
		stage.setTitle("Betting");
		stage.setScene(scene);
		
		TextField amount = new TextField();
		Text amountText = new Text("Please Enter Your Amount To Bet");
		
		
		Text money = new Text("you have : $" + wallet);
		
		Text error = new Text();
		
		Text game = new Text();
		
		Button even = new Button("Even");
		Button odd = new Button("Odd");
		
		HBox amountBox = new HBox();
		amountBox.getChildren().addAll(amount, amountText);
		
		
		VBox overall = new VBox();
		overall.getChildren().addAll(amountBox, money, error, game, even, odd);
		
		root.getChildren().add(overall);
		
		
		/*
		 * this method will set an event handler if the even button is clicked, the event handler will throw
		 * an error message if there is either no input, more input than their wallet, or negative input/0. It will
		 * also put up the message telling them what they rolled that turn, as well as taking away their access
		 * to the game if they go into debt.
		 */
		even.setOnAction(new EventHandler<ActionEvent>(){
			@Override
			public void handle(ActionEvent e) {
				try {
					error.setText("");
					
				if(Integer.parseInt(amount.getText()) > wallet) {
					error.setText("You bet more than your wallet");
				}
				else if(Integer.parseInt(amount.getText()) < 0) {
					error.setText("Nice try buddy, no negative bets!!!");
				}
				else if(Integer.parseInt(amount.getText()) == 0) {
					error.setText("Why would you even want to bet 0?");
				}
				else if(getRoll(1)){
					wallet += Integer.parseInt(amount.getText());
					money.setText("You have : $" + wallet);
					game.setText("You rolled even, you win!!!");
				}
				else {
					wallet -= Integer.parseInt(amount.getText());
					money.setText("You have : $" + wallet);
					game.setText("You rolled odd, you lose :(");
				}
				if(wallet <= 0) {
					error.setText("You have run out of money");
					even.setDisable(true);
					odd.setDisable(true);
				}
				}catch(NumberFormatException r) {
					error.setText("Please enter a value");
				}
			}
		});
		
		
		/*
		 * this method will set an event handler if the odd button is clicked, the event handler will throw
		 * an error message if there is either no input, more input than their wallet, or negative input/0. It will
		 * also put up the message telling them what they rolled that turn, as well as taking away their access
		 * to the game if they go into debt.
		 */
		odd.setOnAction(new EventHandler<ActionEvent>(){
			@Override
			public void handle(ActionEvent e) {
				try {
				if(Integer.parseInt(amount.getText()) > wallet) {
					error.setText("You bet more than your wallet");
				}
				else if(Integer.parseInt(amount.getText()) < 0) {
					error.setText("Nice try buddy, no negative bets!!!");
				}
				else if(Integer.parseInt(amount.getText()) == 0) {
					error.setText("Why would you even want to bet 0?");
				}
				else if(getRoll(2)){
					wallet += Integer.parseInt(amount.getText());
					money.setText("You have : $" + wallet);
					game.setText("You rolled odd, you win!!!");
				}
				else {
					wallet -= Integer.parseInt(amount.getText());
					money.setText("You have : $" + wallet);
					game.setText("You rolled even, you lose :(");
				}
				if(wallet <= 0) {
					error.setText("You have run out of money");
					even.setDisable(true);
					odd.setDisable(true);
				}
				}catch(NumberFormatException r){
					error.setText("Please enter a value");
				}
			}
		});
		
		
		stage.show();
	}
	
	
	
	public static void main(String[] args) {
		Application.launch(args);
	}
	
	/*
	 * the getRoll method takes in a num representing if the user picked even or odd, it then rolls for the user
	 * and returns a boolean representing a win or a loss
	 */
	public static boolean getRoll(int num) {
		int rand = random.nextInt(6);
		rand++;
		if(num == 1) {
			if(rand == 2 || rand == 4 || rand == 6) {
				return true;
			}
			else {
				return false;
			}
		}
		if(num == 2) {
			if(rand == 1 || rand == 3 || rand == 5) {
				return true;
			}
			else {
				return false;
			}
		}
		
		return true;
	}

}
